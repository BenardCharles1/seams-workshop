# Download a zipped collection of files:

- A single python source file with some code  
- Input files that the code uses  
- Output files that the code generates

# TODO: clean up the mess

# Suggested steps:

1. Create a repository. Put something? everything? in it. Set up the repo locally and on gitlab.
2. What problems do you see? Create issues on gitlab.
3. Begin solving the organizational problems, committing with each solution and marking the corresponding issues as solved. 

# Things to consider doing:

- Create a directory hierarchy that reflects the relationships between the files in the projects
- Improve documentation
- Refactor the source file into multiple files so that each file either solves a specific problem, or is a python module to be loaded as needed
- Add/remove files from repo
- Find out how tests should be written in Python, and write some
